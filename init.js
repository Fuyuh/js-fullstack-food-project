const Food = require('./src/models/food')
const food2backFood = (f) => ({
  name: f.name,
  lipids: f.lipides,
  carbohydrates: f.glucides,
  proteins: f.proteines,
  weitht: f.weight
})
const nutriment = (plat, nutriment) => parseFloat(
  Array.from(plat.querySelectorAll('dt'))  // find all dt's in plat
    .find(el => el.innerHTML.toLowerCase() === nutriment) // keep the one with the nutriment
    .nextElementSibling // get to the dd
    .innerHTML) 

const glucides = plat => nutriment(plat, 'glucides')
const lipides = plat => nutriment(plat, 'lipides')
const calories = plat => nutriment(plat, 'calories')
const proteines = plat => nutriment(plat, 'protéines')

const platEl2js = (plat) => new Food({
  name: plat.querySelector('p').innerHTML,
  weight: parseFloat(plat.attributes['data-weight'].value),
  lipides: lipides(plat),
  glucides: glucides(plat),
  proteines: proteines(plat)
})

window.plats = Array.from(document.querySelectorAll('.plat')).map(platEl2js)
window.Food = Food

const backend = 'http://localhost:2999'
plats.map(p => fetch(`${backend}/food`,{
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(food2backFood(p))
}))

