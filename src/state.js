const Food = require('./models/food')
const backFood2food = (bf) => ({
  name: bf.name,
  lipides: bf.lipids,
  glucides: bf.carbohydrates,
  proteines: bf.proteins,
  weight: bf.weight
})

const food2backFood = (f) => ({
  name: f.name,
  lipids: f.lipides,
  carbohydrates: f.glucides,
  proteins: f.proteines,
  weitht: f.weight
})


module.exports = (backend) => {
  let state = new Proxy(
    {
      observers: [],
      views: {'/': []},
      view: '/',
      actions: {},
      addObserver: function(o){this.observers.push(o)}
    },
    {
      set: function(target, prop, val){
        const oldval = target[prop]
        target[prop] = val
        switch (prop) {
        case 'views':
        case 'actions':
        case 'observers':
          break;
        case 'view':
          // hide old components
          target.views[oldval].forEach(c => c.mountPoint.hidden = true)
          console.log(`view changed to ${val}, will display :`)
          console.log(target.views[val])
          // new view 
          target.views[val].forEach(o => { // render then show
            o.render(state)
            o.mountPoint.hidden = false
            //history.replaceState(null, '', val )
          })
          
        }
        return val
      },
      get: function(target, prop){
        switch (prop) {
        case 'target':
          return target
        case 'backend':
          return target.backend
        case 'actions':
          return target.actions
        default:
          return target[prop]
        }
      }
    }
  )

  state.target.backend = backend

  state.actions.loadFood = function(){
    return fetch(`${state.backend}/food`)
      .then(res => res.json())
      .then(list => list.map(p => Object.assign(new Food(backFood2food(p)), {id: p.id})))
      .then(plats => state.plats = plats)
  }

  state.actions.createFood = (food) => {
    console.log(`food creation with ${food}`)
    return fetch(`${state.backend}/food`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(food)
    })
  }

  state.actions.editFood = (id, data) => {
    return fetch(
      `${state.backend}/food/${id}`,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
    )
  }

  state.actions.deleteFood = (targetUrl) => 
    fetch(
    `${state.backend}${targetUrl}`,
    {
      method: 'DELETE'
    }
  )

  state.actions.viewFood = (url) => {
    return fetch(`${state.backend}/food/${url}`,
      {
        method: 'GET'
      } 
    )
    .then(res => res.json())
    .then( plat => state.plat = plat)
  }

  return state

}
