const plats2ul = (plats) => {
  const ul = document.createElement('ul')
  ul.id = "foodlist"
  ul.innerHTML = plats.map(plat2li).join('')
  return ul
}

const plat2li = (plat) => `
  <li class="plat" data-weight="${plat.weight}" data-calories="${plat.caloriesTotal}">
    <p id="${plat.id}">${plat.name} <a data-action="delete" href="/food/${plat.id}" >delete</a></p>
    
  </li>
`

module.exports = (emit, mountPoint) => {
  const Component = require('../component')(state)
  return new Component(
    mountPoint,
    function (state){
      //console.log(state.target.plats)
      this.mountPoint.innerHTML = plats2ul(state.plats).innerHTML
    },
    comp => {
      comp.mountPoint.addEventListener('click', (e) => {
        e.preventDefault()

        if (e.target.matches('p')) {
          //console.log(e.target.getAttribute('id'))
          emit('view resource', e.target.getAttribute('id'))
        }

        if (e.target.matches('a')){
          emit('delete resource', e.target.attributes.href.value)
        }

      })
    }
  )
}
