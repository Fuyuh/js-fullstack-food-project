const plat2detail = (plat) => `
  <div class="plat" data-weight="${plat.weight}" data-calories="${plat.caloriesTotal}">
    <p>${plat.name}</p>
    <p>Glucides : ${plat.carbohydrates}</p>
    <p>Protéines : ${plat.proteins}</p>
    <p>Lipides : ${plat.lipids}</p>
    <a class="edit" data-action="edit" href="/food/${plat.id}" >Edit</a>
    <a class="delete" data-action="delete" href="/food/${plat.id}" >Delete</a>
  </div>
`


module.exports = (emit, mountPoint) => {
    const Component = require('../component')(state)
    return new Component(
        mountPoint,
        function (state) {
            this.mountPoint.innerHTML = plat2detail(state.plat)
        },
        (comp) =>
        comp.mountPoint.addEventListener('click',
        (e) => {
            e.preventDefault()
            if (e.target.matches('a') && e.target.attributes.class.value === 'delete') { // TO OPTIMIZE ?              
                emit('delete resource', e.target.attributes.href.value)
            }

            if (e.target.matches('a') && e.target.attributes.class.value === 'edit') { 
                emit('editfood button clicked', e.target.attributes.href.value)
            }
        },
        true )
        )
}