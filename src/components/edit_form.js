const template = (plat) => `
<input type="text" name="id" size="20" value="${plat.id}" hidden/>
  <label>Nom
    <input type="text" name="name" size="20" value="${plat.name}"/>
  </label>
  <label>Description
    <input type="text" name="description" size="20" value="${plat.description}"/>
  </label>
  <label>Lipides
    <input type="text" name="lipids" size="5" value="${plat.lipids}"/>
  </label>
  <label>Glucides
  <input type="text" name="carbohydrates" size="5" value="${plat.carbohydrates}"/>
  </label>
  <label>Protéines
  <input type="text" name="proteins" size="5" value="${plat.proteins}"/>
  </label>
  <input type="submit" name="submit" value="Modifier" />
`

module.exports = (emit, mountPoint) => {
  const Component = require('../component')(state)
  return new Component(
    mountPoint,

    function (state){
      this.mountPoint.innerHTML = template(state.plat)
    },

    (comp) => {
      comp.mountPoint.addEventListener('submit', (e) => {
        e.preventDefault()
        let editResource = Object.fromEntries(new FormData(comp.mountPoint))
        //console.log(editResource)
        emit('edit resource', editResource)
      })
    }
    
  )
}
