// state
const State = require('./src/state')
const backend = 'http://localhost:2999'
let state = State(backend)
window.state = state

// controller
const controller = require('js-pubsub')
window.controller = controller

//compose UI: list of top elements
const listMountPoint = document.createElement('ul')
const counterMountPoint = document.createElement('div')
const addFoodMountPoint = document.createElement('button')
const createFormMountPoint = document.createElement('form')

const elementMountPoint = document.createElement('div')
const editFormMountPoint = document.createElement('form')

// ---------- COMPONENTS ----------
const listComp = require('./src/components/list')(controller.publish,listMountPoint)
const counterComp = require('./src/components/counter')(controller.publish, counterMountPoint)
const createComp = require('./src/components/addbutton')(controller.publish, addFoodMountPoint)
const createForm = require('./src/components/create_form')(controller.publish, createFormMountPoint)

const elementComp = require('./src/components/view_element')(controller.publish, elementMountPoint)
const editForm = require('./src/components/edit_form')(controller.publish, editFormMountPoint)

const components = [ listComp, counterComp, createComp, createForm, elementComp, editForm]
console.log(state.observers)
components.forEach(comp => state.addObserver(comp))

components.map(c => c.mountPoint).forEach(mp => document.body.appendChild(mp))

// ---------- VIEWS ----------
state.views = {
  '/' : [listComp, counterComp, createComp],
  '/food': [listComp, counterComp, createComp],
  '/food/new': [createForm],
  '/food/:id': [elementComp],
  '/food/edit/:id': [editForm]
}

// ---------- CONTROLLER ----------
controller.subscribe('addfood button clicked', () => {
  state.view = '/food/new'
})
controller.subscribe(
  'init',
  () =>
    state.actions.loadFood()
    .then(() => state.view = '/food')
)
controller.subscribe(
  'create new ressource',
  (newFood) =>
    state.actions.createFood(newFood)
    .then(state.actions.loadFood)
    .then(() => state.view = '/food')
)
controller.subscribe(
  'delete resource',
  (url) =>
    state.actions.deleteFood(url)
    .then(state.actions.loadFood)
    .then(() => state.view = '/food')
)
controller.subscribe(
  'view resource',
  (url) => {
    history.pushState({}, '', `/food/${url}`) // ------------- RIGHT?
    state.actions.viewFood(url)
    .then( () => state.view = '/food/:id')
  }
)
controller.subscribe('editfood button clicked', (root) => {
  history.pushState({}, '', `/edit${root}`)
  state.view = '/food/edit/:id'
}
)
controller.subscribe(
  'edit resource', (editFood) => {
    console.log(editFood)
    state.actions.editFood(editFood.id, editFood)
    .then( () => state.actions.viewFood(editFood.id))
    .then( () => state.view = '/food/:id')
  }
)

// ---------- APPLI INITIALIZATION ----------
controller.publish('init') // initial action, fetch data, decide first view
window.addEventListener('popstate', (e) => {
  console.log(location.pathname)
  state.view = location.pathname 
})
